/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["images.pexels.com"],
  },
  assetPrefix: process.env.NODE_ENV === 'production' ? '/name-of-repository' : '',
}

module.exports = nextConfig